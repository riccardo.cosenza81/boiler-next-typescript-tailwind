import type { NextPage } from 'next';
import Posts from '../components/Posts';
import Footer from '../components/Footer';

const Home: NextPage = () => {
  return (
    <div className="container mx-auto">
      <div>
        <Posts />
      </div>
      <Footer />
    </div>
  );
};

export default Home;
