import styles from '../styles/Home.module.css';

function Footer() {
  return (
    <footer className={styles.footer}>
      <a href="mailto:riccardo.cosenza81@gmail.com" target="_blank" rel="noopener noreferrer">
        Powered by <span style={{ marginLeft: '5px' }}>Riccardo Cosenza</span>
      </a>
    </footer>
  );
}

export default Footer;
