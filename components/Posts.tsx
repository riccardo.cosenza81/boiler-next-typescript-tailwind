import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Post as PostType } from '../interfaces';

function Posts() {
  const defaultPosts: PostType[] = [];
  const [posts, setPosts]: [PostType[], (posts: PostType[]) => void] = useState(defaultPosts);

  useEffect(() => {
    axios.get<PostType[]>('https://jsonplaceholder.typicode.com/posts').then((response) => {
      setPosts(response.data);
    });
  }, []);

  return (
    <div>
      {posts.map((post) => (
        <li key={post.id}>
          <h3>{post.title}</h3>
          <p>{post.body}</p>
        </li>
      ))}
    </div>
  );
}

export default Posts;
